import '../polyfills';
import './modalLogic';
import Vue        from 'vue';
import App        from './App.vue';
import setStorage from './setStorage';

const $ = window.jQuery;

new Vue( {
	el: '#gdpr-visitor-consent-app',
	render: h => h( App ),
} );

$( '#gdpr-visitor-consent-banner .accept' ).click( function handleAcceptClick() {
	setStorage( getAllSlugs() );
} );

// Display the banner if storage is not set and no shortcode exists
if ( ! window.localStorage.getItem( 'gdprvc-storage' ) &&
	 ! $( '#gdpr-visitor-consent-shortcode' ).length ) {
	$( '#gdpr-visitor-consent-banner' ).show();
}

// Display the banner and reconsent message (if any) if force reconsent
const storage = window.localStorage.getItem( 'gdprvc-storage' );
if ( storage &&
	 storage.split( ',' )[0] < window.GDPR_VISITOR_CONSENT_FRONTEND.reconsent ) {
	if ( $( '#gdpr-visitor-consent-banner .reconsent' ).length ) {
		$( '#gdpr-visitor-consent-banner p' ).hide();
		$( '#gdpr-visitor-consent-banner .reconsent' ).show();
	}

	$( '#gdpr-visitor-consent-banner' ).show();
}

// Returns all script slugs as a comma-separated string
function getAllSlugs() {
	let slugs = [];

	window.GDPR_VISITOR_CONSENT_FRONTEND.options.forEach( ( group ) => {
		group.scripts.forEach( ( script ) => {
			slugs.push( script.slug );
		} );
	} );

	return slugs.join( ',' );
}
