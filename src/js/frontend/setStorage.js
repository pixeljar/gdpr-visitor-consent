export default function( value ) {

	// Prepend timestamp to value
	value = Math.round( Date.now() / 1000 ) + ',' + value;
	window.localStorage.setItem( 'gdprvc-storage', value );
	window.location.reload();

}
