// Handle the hide/display of modal
const $ = window.jQuery;
const fadeTiming = 100;

// Show the modal and form on "Cookie Settings" button click
$( '#gdpr-visitor-consent-banner .settings' ).click( fadeIn );

$( '#gdpr-visitor-consent-form .overlay, #gdpr-visitor-consent-form .close' ).click( fadeOut );

function fadeIn() {
	$( '#gdpr-visitor-consent-form' ).fadeIn( fadeTiming );
}

function fadeOut() {
	$( '#gdpr-visitor-consent-form' ).fadeOut( fadeTiming );
}
