export default function() {
	const groups = window.GDPR_VISITOR_CONSENT_DATA.options;
	let slugs = [];

	groups.forEach( ( group ) => {
		group.scripts.forEach( ( script ) => {
			slugs.push( script.slug );
		} );
	} );

	return slugs;
}
