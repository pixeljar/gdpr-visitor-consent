const webpack              = require( 'webpack' );
const inProduction         = process.env.NODE_ENV === 'production';
const WebpackCleanPlugin   = require( 'webpack-clean' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const VueLoaderPlugin      = require('vue-loader/lib/plugin')

const entry = {
	'js/frontend'  : './src/js/frontend',
	'js/admin'     : './src/js/admin',
	'css/frontend' : './src/css/frontend.scss',
	'css/admin'    : './src/css/admin.scss',
};

module.exports = {
	entry,
	module: {
		rules: [
			{
				test: /\.js$/,
				use: 'babel-loader',
			},
			{
				test: /\.(png|jpg|svg|eot|ttf|woff)$/,
				use: [
					{
						loader: 'url-loader', options: {
							limit: 10000
						},
					},
				],
			},
			{
				test: /\.(css|scss)$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader', options: {
							minimize: inProduction,
						},
					},
					'sass-loader',
				],
			},

			{
				test: /\.vue$/,
				use: 'vue-loader',
			},

		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "[name].css",
		}),
		new WebpackCleanPlugin(
			// Array of JS files in the CSS folder
			Object.keys( entry )
				.filter( ( key ) => key.startsWith( 'css' ) )
				.map( ( key ) => `dist/${key}.js` )
		),
		new VueLoaderPlugin()
	],
	mode: inProduction ? 'production' : 'development',
};
