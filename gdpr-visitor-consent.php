<?php
/*
Plugin Name: GDPR Visitor Consent
Description: Easily allow users to have control of which scripts they allow.
Plugin URI: https://www.pixeljar.com
Author: Pixel Jar
Author URI: http://www.pixeljar.com
Version: 1.1.4
License: GPL2
Text Domain: gdpr-visitor-consent
Domain Path: /lang

    Copyright (C) Feb 26, 2017  Pixel Jar  info@pixeljar.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( ! defined( 'ABSPATH' ) ) { die( 'You are not allowed to call this file directly.' ); }

define( 'GDPR_VISITOR_CONSENT_SLUG', 'gdpr-visitor-consent' );
define( 'GDPR_VISITOR_CONSENT_VERSION', '1.1.4' );

define( 'GDPR_VISITOR_CONSENT_PATH',      plugin_dir_path( __FILE__ ) );
define( 'GDPR_VISITOR_CONSENT_URL',       plugin_dir_url( __FILE__ ) );
define( 'GDPR_VISITOR_CONSENT_LIB',       GDPR_VISITOR_CONSENT_PATH . 'lib/' );
define( 'GDPR_VISITOR_CONSENT_ABSTRACTS', GDPR_VISITOR_CONSENT_LIB . 'abstracts/' );
define( 'GDPR_VISITOR_CONSENT_VIEWS',     GDPR_VISITOR_CONSENT_PATH . 'views/' );
define( 'GDPR_VISITOR_CONSENT_DIST',      GDPR_VISITOR_CONSENT_URL . 'dist/' );
define( 'GDPR_VISITOR_CONSENT_JS',        GDPR_VISITOR_CONSENT_DIST . 'js/' );
define( 'GDPR_VISITOR_CONSENT_CSS',       GDPR_VISITOR_CONSENT_DIST . 'css/' );
define( 'GDPR_VISITOR_CONSENT_SRC_URL',   GDPR_VISITOR_CONSENT_URL . 'src/' );

require_once( GDPR_VISITOR_CONSENT_LIB . 'admin-screen.php' );
require_once( GDPR_VISITOR_CONSENT_LIB . 'banner.php' );
require_once( GDPR_VISITOR_CONSENT_LIB . 'options.php' );
require_once( GDPR_VISITOR_CONSENT_LIB . 'parse-script.php' ); // Do not rearrange
require_once( GDPR_VISITOR_CONSENT_LIB . 'scripts.php' );
