<?php

/**
 * Loads scripts in the head and footer
 */

require_once( GDPR_VISITOR_CONSENT_ABSTRACTS . 'get-options.php' );

class GDPR_VISITOR_CONSENT_Scripts extends GDPR_VISITOR_CONSENT_Get_Options {

	function __construct() {}

	/**
	 * Initialize this class
	 */
	public static function init() {

		parent::init();
		self::hooks();

	}

	/**
	 * Adds actions and filters for this class
	 */
	public static function hooks() {

		add_action( 'wp_head', array( __CLASS__, 'wp_head' ) );
		add_action( 'wp_footer', array( __CLASS__, 'wp_footer' ) );

	}

	public static function wp_head() {

		self::enable_src();
		self::print_scripts( 'wp_head' );

	}

	public static function wp_footer() {

		self::print_scripts( 'wp_footer' );

	}

	/**
	 * Print scripts for this hook
	 */
	public static function print_scripts( $hook ) {

		// Loop through each group
		foreach ( self::$options as $group ) {

			if ( ! isset( $group['scripts'] ) || ! is_array( $group['scripts'] ) ) {
				continue;
			}

			// Loop through each script in a group
			foreach ( $group['scripts'] as $script ) {

				// if this script has the right hook
				// and is not empty, parse and echo
				if ( $script['code'] && $hook === $script['location'] ) {
					echo gdprvc_parse_script( $script['code'], $script['slug'] );
				}

			}

		}

	}

	/**
	 * Print a script tag, before everything else,
	 * that replaces src tags on divs with the class
	 * of gdprvc-src-replace, based on the data-slug
	 */
	public static function enable_src() {
	?>

		<script>
			function gdprvc_is_accepted( slug ) {
				// Get user preferences
				var userPreferences = localStorage.getItem( 'gdprvc-storage' );
				// If not set, false
				if ( ! userPreferences ) {
					return false;
				}
				// If in preferences, true
				if ( userPreferences.indexOf( slug ) !== -1 ) {
					return true;
				}
				return false;
			}

			document.addEventListener( 'DOMContentLoaded', function () {
				// Get all scripts inserted without src
				var elementsWithSrc = document.querySelectorAll( '.gdprvc-src-replace' );
				// Loop through all those elements
				for ( var i = 0; i < elementsWithSrc.length; i++ ) {
					// Get the slug
					var slug = elementsWithSrc[ i ].getAttribute( 'data-gdprvc-slug' );
					// If user has not accepted script, return
					if ( ! gdprvc_is_accepted( slug ) ) {
						return;
					}
					// Replace the src
					var src = elementsWithSrc[ i ].getAttribute( 'data-gdprvc-src' );
					elementsWithSrc[ i ].src = src;
				}
			} );
		</script>

	<?php
	}

}

GDPR_VISITOR_CONSENT_Scripts::init();
