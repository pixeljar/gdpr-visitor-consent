<?php
/**
 * Abstract class that retrieves options
 */

abstract class GDPR_VISITOR_CONSENT_Get_Options {

	public static $options_name = GDPR_VISITOR_CONSENT_SLUG . '-options';
	public static $other_options_name = GDPR_VISITOR_CONSENT_SLUG . '-other-options';

	public static $options;
	public static $other_options;

	/**
	 * Initialize the class
	 */
	public static function init() {

		self::get_options();

	}

	/**
	 * Retrieve the options for this plugin
	 */
	public static function get_options() {

		self::$options = get_option( self::$options_name );
		if ( ! self::$options ) {
			self::$options = array();
		}

		self::$options = self::unslash_scripts( self::$options );

		self::$other_options = get_option( self::$other_options_name );
		if ( ! self::$other_options ) {
			self::$other_options = array();
		}

	}

	/**
	 * Unslash the scripts
	 */
	public static function unslash_scripts( $options ) {

		if ( ! count( $options ) ) {
			return $options;
		}

		foreach ( $options as $option_key => $option ) {
			if ( ! array_key_exists( 'scripts', $option ) ) {
				$options[ $option_key ]['scripts'] = array();
				continue;
			}

			foreach ( $option['scripts'] as $script_key => $script ) {
				if ( ! array_key_exists( 'code', $script ) ) {
					continue;
				}

				$options[ $option_key ]['scripts'][ $script_key ]['code'] = wp_unslash( $script['code'] );
			}
		}

		return $options;

	}

}
