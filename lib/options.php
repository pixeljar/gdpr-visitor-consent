<?php

/**
 * Sets up and renders the admin screen
 */

require_once( GDPR_VISITOR_CONSENT_ABSTRACTS . 'get-options.php' );

class GDPR_VISITOR_CONSENT_Options extends GDPR_VISITOR_CONSENT_Get_Options {

	function __construct() {}

	/**
	 * Initialize this class
	 */
	public static function init() {

		parent::init();
		self::hooks();

	}

	/**
	 * Adds actions and filters for this class
	 */
	public static function hooks() {

		add_action( 'admin_init', array( __CLASS__, 'register_setting' ) );

	}

	public static function register_setting() {

		add_settings_section(
			GDPR_VISITOR_CONSENT_SLUG,
			__( 'GDPR Cookies', 'gdpr-visitor-consent' ),
			array( __CLASS__, 'section_callback' ),
			'toplevel_page_gdpr-visitor-consent'
		);

		register_setting(
			GDPR_VISITOR_CONSENT_SLUG,
			self::$options_name,
			array(
				'sanitize_callback' => array( __CLASS__, 'sanitize_options' ),
			)
		);

		register_setting(
			GDPR_VISITOR_CONSENT_SLUG,
			self::$other_options_name,
			array(
				'sanitize_callback' => array( __CLASS__, 'sanitize_other_options' ),
			)
		);

	}

	public static function section_callback() {}

	public static function sanitize_options( $dirty ) {

		$clean = array();

		if ( ! is_array( $dirty ) ) {
			return $clean;
		}

		foreach( $dirty as $index => $group ) {

			// Details for this group
			$clean[ $index ]['name']        = sanitize_text_field( $group['name'] );
			$clean[ $index ]['description'] = sanitize_text_field( $group['description'] );
			$clean[ $index ]['locked']      = $group['locked'] === 'true' ? true : false;

			if ( array_key_exists( 'scripts', $group ) && is_array( $group['scripts'] ) ) {

				$clean[ $index ]['scripts'] = array();

				// Loop through each script in this group
				foreach ( $group['scripts'] as $script ) {

					$script_options = array(
						'name'             => sanitize_text_field( $script['name'] ),
						'purpose'          => sanitize_text_field( $script['purpose'] ),
						'location'         => $script['location']  === 'wp_footer' ? 'wp_footer' : 'wp_head',
						'code'             => wp_slash( $script['code'] ),
						'slug'             => sanitize_text_field( $script['slug'] ),
					);

					if ( isset( $script['thirdPartyActive'] ) ) {
						$script_options['thirdPartyActive'] = 'true' === $script['thirdPartyActive'] ? 1 : 0;
					}

					if ( isset( $script['thirdPartySlug'] ) ) {
						$script_options['thirdPartySlug'] = sanitize_text_field( $script['thirdPartySlug'] );
					}

					array_push( $clean[ $index ]['scripts'], $script_options );
				}
			}

		}

		return $clean;

	}

	public static function sanitize_other_options( $dirty ) {

		$clean = array();
		$clean['message']          = sanitize_textarea_field( $dirty['message'] );
		$clean['reconsentMessage'] = sanitize_textarea_field( $dirty['reconsentMessage'] );
		$clean['active']           =
			( $dirty['active'] && $dirty['active'] !== 'false' )
			? true : false;

		if ( 'true' === $dirty['lastReconsent'] ) {
			$clean['lastReconsent'] = time();
		} else {
			if ( array_key_exists( 'lastReconsent', self::$other_options ) ) {
				$clean['lastReconsent'] = self::$other_options['lastReconsent'];
			} else {
				$clean['lastReconsent'] = 0;
			}
		}

		return $clean;

	}

}

GDPR_VISITOR_CONSENT_Options::init();
