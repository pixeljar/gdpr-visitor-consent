<?php

/**
 * Sets up and renders the admin screen
 */

require_once( GDPR_VISITOR_CONSENT_ABSTRACTS . 'get-options.php' );

class GDPR_VISITOR_CONSENT_Admin_Screen extends GDPR_VISITOR_CONSENT_Get_Options {

	function __construct() {}

	/**
	 * Initialize this class
	 */
	public static function init() {

		parent::init();
		self::hooks();

	}

	/**
	 * Adds actions and filters for this class
	 */
	public static function hooks() {

		add_action( 'admin_menu', array( __CLASS__, 'admin_page' ) );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ), 10, 1 );

	}

	/**
	 * Renders the admin page for PJ GDPR
	 */
	public static function admin_page() {

		add_menu_page(
			__( 'GDPR Visitor Consent', 'gdpr-visitor-consent' ),
			__( 'GDPR Visitor Consent', 'gdpr-visitor-consent' ),
			'manage_options',
			'gdpr-visitor-consent',
			array( __CLASS__, 'render_admin_page' ),
			'dashicons-shield-alt',
			10
		);

	}

	/**
	 * Render the admin page
	 */
	public static function render_admin_page() {

		require_once( GDPR_VISITOR_CONSENT_VIEWS . 'admin-screen.php' );

	}

	/**
	 * Enqueue styles and scripts
	 */
	public static function enqueue_scripts( $slug ) {

		if ( 'toplevel_page_gdpr-visitor-consent' !== $slug ) {
			return;
		}

		wp_enqueue_script(
			GDPR_VISITOR_CONSENT_SLUG . '-admin',
			GDPR_VISITOR_CONSENT_JS . 'admin.js',
			array( 'jquery' ),
			GDPR_VISITOR_CONSENT_VERSION,
			true
		);

		// Google fonts/icons for Vuetify
		wp_enqueue_style(
			GDPR_VISITOR_CONSENT_SLUG . '-google',
			'https://fonts.googleapis.com/css?family=Material+Icons'
		);

		wp_enqueue_style(
			GDPR_VISITOR_CONSENT_SLUG . '-admin',
			GDPR_VISITOR_CONSENT_CSS . 'admin.css',
			array( GDPR_VISITOR_CONSENT_SLUG . '-google' ),
			GDPR_VISITOR_CONSENT_VERSION,
			'all'
		);

		//
		// array with labels and slugs of third party scripts
		// array(
		//     'label' => 'Label'
		//     'slug'  => 'slug-slug',
		// );
		//
		$third_party_scripts = apply_filters( 'gdprvc_third_party_scripts', array() );

		// Pass data to the admin script
		wp_localize_script( GDPR_VISITOR_CONSENT_SLUG . '-admin', 'GDPR_VISITOR_CONSENT_DATA', array(
			'optionsName'       => self::$options_name,
			'options'           => self::$options,
			'otherOptionsName'  => self::$other_options_name,
			'otherOptions'      => self::$other_options,
			'thirdPartyScripts' => $third_party_scripts,
		) );

		// Make strings available to the admin script
		wp_localize_script( GDPR_VISITOR_CONSENT_SLUG . '-admin', 'GDPR_VISITOR_CONSENT_TEXT', array(
			'title'                  => __( 'GDPR Visitor Consent', 'gdpr-visitor-consent' ),
			'save'                   => __( 'Save', 'gdpr-visitor-consent' ),
			'reconsent'              => __( 'Force users to re-consent' ),
			'label'                  => __( 'label', 'gdpr-visitor-consent' ),
			'description'            => __( 'description', 'gdpr-visitor-consent' ),
			'scripts'                => __( 'Scripts:', 'gdpr-visitor-consent' ),
			'code'                   => __( 'code (optional - only <script>, <noscript>, and <img> tags allowed)', 'gdpr-visitor-consent' ),
			'scriptName'             => __( 'script name', 'gdpr-visitor-consent' ),
			'purpose'                => __( 'purpose', 'gdpr-visitor-consent' ),
			'location'               => __( 'location (optional)', 'gdpr-visitor-consent' ),
			'addGroup'               => __( 'Add Group', 'gdpr-visitor-consent' ),
			'locked'                 => __( 'Locked (user cannot disabled)', 'gdpr-visitor-consent' ),
			'unlocked'               => __( 'Unlocked (user can disable)', 'gdpr-visitor-consent' ),
			'slug'                   => __( 'slug (do not edit after creation)', 'gdpr-visitor-consent' ),
			'slugValidation'         => __( 'must be lowercase and dashes (-)', 'gdpr-visitor-consent' ),
			'selectScript'           => __( 'select a third-party script (optional)', 'gdpr-visitor-consent' ),
			// "Other Settings" tab
			'otherSettings'          => __( 'Other Settings', 'gdpr-visitor-consent' ),
			'bannerMessage'          => __( 'banner message', 'gdpr-visitor-consent' ),
			'bannerActive'           => __( 'banner active', 'gdpr-visitor-consent' ),
			'bannerInactive'         => __( 'banner inactive', 'gdpr-visitor-consent' ),
			'whenBuilding'           => __( 'leave this unchecked until you are finished building the scripts', 'gdpr-visitor-consent' ),
			'reconsentBannerMessage' => __( 'banner message for users who have to re-consent (optional)', 'gdpr-visitor-consent' ),
		) );

	}

}

GDPR_VISITOR_CONSENT_Admin_Screen::init();
