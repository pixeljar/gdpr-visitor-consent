<?php

/**
 * Parses a script
 */

function gdprvc_parse_script( $script, $slug ) {

	// Eliminate all src on elements

	// The regex patterns for src attributes
	$src_patterns = array(
		'/(src\=\")(.*?)(\")/',
		"/(src\=\')(.*?)(\')/",
	);

	// The string to replace src with
	$src_replacement = 'data-gdprvc-src="${2}" class="gdprvc-src-replace" data-gdprvc-slug="' . esc_attr( $slug ) . '"';

	$script = preg_replace( $src_patterns, $src_replacement, $script );

	// Wrap inner code of script tag in a conditional

	// The string to replace script element with
	$script_str = '${1}
	document.addEventListener( "DOMContentLoaded", function() {
	if ( window.gdprvc_is_accepted( "' . esc_js( $slug ) . '" ) ) {
	${2}
	}
	} );
	${3}';
	$script_pattern = '/(\<script.*?\>)(.*?)(\<\/script\>)/s';
	$script = preg_replace( $script_pattern, $script_str, $script );

	return $script;

}
