<?php

/**
 * Displays the banner and modal
 */

require_once( GDPR_VISITOR_CONSENT_ABSTRACTS . 'get-options.php' );

class GDPR_VISITOR_CONSENT_Banner extends GDPR_VISITOR_CONSENT_Get_Options {

	function __construct() {}

	/**
	 * Initialize this class
	 */
	public static function init() {

		parent::init();
		self::hooks();

	}

	/**
	 * Adds actions and filters for this class
	 */
	public static function hooks() {

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'register_scripts' ) );
		add_action( 'wp_footer', array( __CLASS__, 'banner' ) );
		add_action( 'init', array( __CLASS__, 'add_shortcode' ) );

	}

	/**
	 * Register the scripts
	 */
	public static function register_scripts() {

		wp_register_script(
			GDPR_VISITOR_CONSENT_SLUG . '-frontend',
			GDPR_VISITOR_CONSENT_JS . 'frontend.js',
			array( 'jquery' ),
			GDPR_VISITOR_CONSENT_VERSION,
			true
		);

		wp_register_style(
			GDPR_VISITOR_CONSENT_SLUG . '-frontend',
			GDPR_VISITOR_CONSENT_CSS . 'frontend.css',
			array(),
			GDPR_VISITOR_CONSENT_VERSION,
			'all'
		);

		wp_localize_script( GDPR_VISITOR_CONSENT_SLUG . '-frontend', 'GDPR_VISITOR_CONSENT_FRONTEND', array(
			'options'   => self::$options,
			'reconsent' => self::$other_options['lastReconsent'],
			'labels'    => array(
				'saveSettings' => __( 'Save Settings', 'gdpr-visitor-consent' ),
				'details'      => __( 'Details', 'gdpr-visitor-consent' ),
				'name'         => __( 'Name', 'gdpr-visitor-consent' ),
				'purpose'      => __( 'Purpose', 'gdpr-visitor-consent' ),
				'select'       => __( 'Select', 'gdpr-visitor-consent' ),
			),
		) );

	}

	/**
	 * Display the banner on the frontend
	 */
	public static function banner() {

		// Only display if enabled
		if ( ! array_key_exists( 'active', self::$other_options ) ||
			 ! self::$other_options['active'] ) {
			return;
		}

		wp_enqueue_style( GDPR_VISITOR_CONSENT_SLUG . '-frontend' );
		wp_enqueue_script( GDPR_VISITOR_CONSENT_SLUG . '-frontend' );

		$reconsentMessage = self::$other_options['reconsentMessage'];
		$message = self::$other_options['message'];

		require_once( GDPR_VISITOR_CONSENT_VIEWS . 'frontend-form.php' );
		require_once( GDPR_VISITOR_CONSENT_VIEWS . 'frontend-banner.php' );

	}

	public static function add_shortcode() {

		add_shortcode( 'gdpr_visitor_consent', array( __CLASS__, 'shortcode_view' ) );

	}

	public static function shortcode_view() {

		wp_enqueue_style( GDPR_VISITOR_CONSENT_SLUG . '-frontend' );
		wp_enqueue_script( GDPR_VISITOR_CONSENT_SLUG . '-frontend' );

		ob_start();
		echo '<div id="gdpr-visitor-consent-shortcode">';
			require_once( GDPR_VISITOR_CONSENT_VIEWS . 'frontend-form.php' );
		echo '</div>';
		return ob_get_clean();

	}

}

GDPR_VISITOR_CONSENT_Banner::init();
