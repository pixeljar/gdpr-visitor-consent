=== GDPR Visitor Consent ===
Contributors: === GDPR Visitor Consent ===
Contributors: nateconley, brandondove, jeffreyzinn
Tags: gdpr, privacy, scripts
Requires at least: 3.9
Tested up to: 5.6
Requires PHP: 5.6
Stable tag: 1.1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allow users to have control of what scripts are loaded.

== Screenshots ==

1. The front-end screen a user sees to configure their preferences.
2. The user preferences as a shortcode, embedded into a page.
3. The admin script editing interface.
4. The admin script editing interface.

== Description ==

Allow users to have control of what scripts are loaded.

Scripts are grouped together (Required, Marketing, Analytics, etc). You can also lock a group, so that a user cannot disable a script within that group (think WordPress or Stripe cookies).

We strongly encourage you to perform an audit of you site to find out what scripts may be tracking users. Scripts that track users could include such things as: analytics scripts, CRM scripts, tracking pixels, and more.

Use the shortcode `[gdpr_visitor_consent]` to create a user preferences page. Using this shortcode, a user can change their GDPR preferences at a later date.

## Developer API

You can add your script in a dropdown in the admin screen using the filter `gdprvc_third_party_script( $scripts );`

**Example (PHP):**

`function add_third_party_script( $scripts ) {

	$scripts[] = array(
		'slug'  => 'third-party-script',
		'label' => 'Script Label',
	);

	return $scripts;

}
add_filter( 'gdprvc_third_party_scripts', 'add_third_party_script', 10, 1 );
`

*You can then use the JavaScript API in the browser to control your scripts on the front-end*

**Example (JavaScript):**

`window.gdprvc_is_accepted( 'slug' );

// returns true/false`

You can also use our helper function to parse your script containing `<script>` and `<img>` tags. This will automatically place your JavaScript in a conditional and remove `src` attributes until a user has consented.

**Example (PHP):**

`
add_action( 'wp_head', 'your_wp_head' );

function your_wp_head() {
	// Check for GDPR Visitor Consent Plugin
	if ( function_exists( 'gdprvc_parse_script' ) ) {
		echo gdprvc_parse_script( 'your script as a string', 'slug' );
		return;
	}
	// Echo normally if plugin is not active
	echo 'your script as a string';
}
`

## Browser Compatibility

**Chrome**
	✓ Admin Editing
	✓ Front-end functionality
	✓ Front-end styles

**Firefox**
	✓ Admin Editing
	✓ Front-end functionality
	✓ Front-end styles

**Safari**
	✓ Admin Editing
	✓ Front-end functionality
	✓ Front-end styles

**Edge**
	✓ Admin Editing
	✓ Front-end functionality
	✓ Front-end styles

**IE11**
	✘ Admin Editing
	✓ Front-end functionality
	✓ Front-end styles

**IE10**
	✘ Admin Editing
	✓ Front-end functionality
	✘ Front-end styles

**IE9**
	✘ Admin Editing
	✓ Front-end functionality
	✘ Front-end styles

== Installation ==

1. Upload `gdpr-visitor-consent.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Navigate to 'GDPR Visitor Consent' in the sidebar and configure your scripts
4. Switch the banner to active in 'Other Settings' and Save!

== Frequently Asked Questions ==

= Does this plugin control scripts from other plugins?

GDPR Visitor Consent provides an API for third-party developers to integrate their scripts into this plugin.

= What happens if I add a new script after I have already published my scripts?

Simply check "Force users to re-consent" before saving your scripts. This will force users who have already saved their preferences to opt-in to your new scripts. Re-consenting still loads a user's previous preferences.

= Will this work with page caching? =

As of version 1.1.0, yes! GDPR Visitor Consent is JavaScript-based and uses the localStorage API.

= What html tags are supported? =

You may insert `<script>` tags and `<img>` tags. Sometimes, `<img>` tags are surrounded by `<noscript>`. This works, too.

== Changelog ==

= 1.0.0 =
* Initial Launch

= 1.1.0 =
* Significant updates to the core functionality of the plugin. This plugin now uses localStorage instead of a cookie, in order to work when page caching is enabled.
* Breaking changes to the Developer API

= 1.1.1 =
* Adds in a missing file causing fatal error for some users.

= 1.1.2 =
* Fixes an error in the display of shortcodes.

= 1.1.3 =
* Prevents errors if there are no scripts added to a group

= 1.1.4 =
* Streamlines JavaScript polyfills

== Upgrade Notice ==

= 1.1.0 =
This update introduces breaking changes to the Developer API. We now use localStorage instead of cookies in order be compatible with page caching.
