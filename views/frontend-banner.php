<div id="gdpr-visitor-consent-banner" style="display:none">
	<div class="inner">
		<?php do_action( 'gdprvc_before_banner_content' ); ?>
		<p><?php echo $message; ?></p>
		<?php
			if ( $reconsentMessage ) {
				printf( '<p style="display:none" class="reconsent">%s</p>',
					$reconsentMessage
				);
			}
		?>
		<button class="settings"><?php
			echo apply_filters(
				'gdprvc_cookie_settings_button_text',
				esc_html__( 'Cookie Settings', 'gdpr-visitor-consent' )
			);
		?></button>
		<button class="accept"><?php
			echo apply_filters(
				'gdprvc_accept_settings_button_text',
				esc_html__( 'Accept Settings', 'gdpr-visitor-consent' )
			);
		?></button>
		<?php do_action( 'gdprvc_after_banner_content' ); ?>
	</div>
</div>