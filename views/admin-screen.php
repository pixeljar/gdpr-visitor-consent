<div class="wrap" id="gdpr-visitor-consent">
	<!-- This hidden h2 forces admin notice placement -->
	<h2 style="opacity:0;height:0;"><?php esc_html_e( 'GDPR Visitor Consent' ); ?></h2>
	<form action="options.php" method="POST">
		<div id="app"></div>
		<?php
			settings_fields( GDPR_VISITOR_CONSENT_SLUG );
			do_settings_sections( GDPR_VISITOR_CONSENT_SLUG );
		?>
	</form>

	<div class="pixel-jar-ads">
		<h3><?php esc_html_e( 'More from Pixel Jar', 'gdpr-visitor-consent' ); ?></h3>

		<div class="ad-flex-container">
			<div class="ad">
				<a href="https://www.pixeljar.com/" target="_blank">
					<img
						src="<?php echo GDPR_VISITOR_CONSENT_SRC_URL . 'images/pixel-jar.svg' ?>" 
						alt="<?php esc_attr_e( 'Pixel Jar logo', 'gdpr-visitor-consent' ); ?>"
					/>
				</a>
				<p><?php
					printf(
						esc_html__( '%1$s is proudly powered by %2$s. We’re a small web development agency that focuses on %3$s as a development platform for websites. %2$s started in 2004. It grew out of the desire to be free to choose projects that challenge us and work with clients that inspire us.', 'gdpr-visitor-consent' ),
						'GDPR Visitor Consent',
						'Pixel Jar',
						'WordPress'
					);
				?></p>
			</div>

			<div class="ad">
				<a href="https://adsanityplugin.com/" target="_blank">
					<img
						src="<?php echo GDPR_VISITOR_CONSENT_SRC_URL . 'images/adsanity.svg' ?>" 
						alt="<?php esc_attr_e( 'AdSanity logo', 'gdpr-visitor-consent' ); ?>"
					/>
				</a>
				<p><?php
					printf(
						wp_kses(
							__( '%1$s also makes %2$s, a light ad rotator plugin for %3$s. It allows the user to create and manage ads shown on a website as well as keep statistics on views and clicks through a robust set of features. You can read all about on the <a href="%4$s" target="_blank">%2$s site</a>.', 'gdpr-visitor-consent' ),
							array( 'a' => array(
								'href'   => array(),
								'target' => array(),
							) )
						),
						'Pixel Jar',
						'AdSanity',
						'WordPress',
						esc_url( 'https://adsanityplugin.com/' )
					);
				?></p>
			</div>

			<div class="ad">
				<a href="https://www.clickrangerpro.com/" target="_blank">
					<img
						src="<?php echo GDPR_VISITOR_CONSENT_SRC_URL . 'images/click-ranger-pro.svg' ?>"
						alt="<?php esc_attr_e( 'Click Ranger Pro logo', 'gdpr-visitor-consent' ); ?>"
					/>
				</a>
				<p><?php
					printf(
						esc_html__( '%s helps you easily track user clicks, downloads, and events of your %s website to %s. Get the data you need without the fuss of JavaScript or PHP.', 'gdpr-visitor-consent' ),
						'Click Ranger Pro',
						'WordPress',
						'Google Analytics'
					);
				?></p>
			</div>
		</div>
	</div>
</div>