<div id="gdpr-visitor-consent-form">
	<div class="overlay"></div>
	<div class="form">
		<?php do_action( 'gdprvc_before_form_content' ); ?>
		<h3><?php
			echo apply_filters(
				'gdprvc_cookie_settings_form_text',
				esc_html__( 'Cookie Settings', 'gdpr-visitor-consent' )
			);
		?></h3>
		<div id="gdpr-visitor-consent-app"></div>
		<button class="close" aria-label="<?php esc_attr_e( 'Close form modal', 'gdpr-visitor-consent' ); ?>">
			<svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"></path></svg>
		</button>
		<?php do_action( 'gdprvc_after_form_content' ); ?>
	</div>
</div>
